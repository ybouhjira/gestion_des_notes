TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += liste.c \
    main.c \
    fichier.c \
    etudiant.c

HEADERS += \
    liste.h \
    etudiant.h \
    date.h \
    fichier.h

