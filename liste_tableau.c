#include "liste.h"

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>

#define TAILLE_MAX 100

struct Liste
{
    Etudiant donnees[TAILLE_MAX];
    int dernier_element;
};

Liste* Creer_liste()
{
    Liste *liste = malloc(sizeof(Liste));
    liste->dernier_element = -1;
    return liste;
}

int Taille_liste(Liste *liste)
{
    return liste->dernier_element + 1;
}

void Inserer_liste(Liste *liste, Etudiant valeur, int position)
{
    assert(position <= liste->dernier_element + 1);
    assert(position >= 0);

    // Décalage
    int i;
    for (i = liste->dernier_element + 1; i > position; --i)
        Copie_etudiant(liste->donnees[i - 1], &liste->donnees[i]);

    // Insertion
    Copie_etudiant(valeur, &liste->donnees[position]);

    // Incrémenter la taille de la liste
    liste->dernier_element++;
}

Etudiant* Acceder_liste(Liste *liste, int position)
{
    assert(position >= 0);
    assert(position < liste->dernier_element + 1);
    return &liste->donnees[position];
}

void Supprimer_liste(Liste *liste, int position)
{
    assert(position >= 0);
    assert(position < liste->dernier_element + 1);

    // décalage
    int i;
    for (i = position; i < liste->dernier_element; ++i)
        Copie_etudiant(liste->donnees[i+1], &liste->donnees[i]);

    // décrementer la taille
    liste->dernier_element--;
}

void Detruire_liste(Liste *liste)
{
    free(liste);
}

Etudiant* Localiser_etudiant(Liste *liste, char *cne)
{
    int i;
    for (i = 0; i < liste->dernier_element + 1; ++i)
        if(!strcmp(liste->donnees[i].cne, cne)) return &liste->donnees[i];
    return NULL;
}
